# oowlish-challenge

Oowlish React/Node.js/JS Challenge.

## Getting Started

This repository is orchestrated with yarn & yarn workspaces, making use of the monorepo standard to be able to share common code between different services and apps.

Please make sure you have [Nodejs](https://nodejs.org/en/download/) & [Yarn](https://classic.yarnpkg.com/en/docs/getting-started) installed before continuing.

Postman collection is provided inside the `docs` directory.

### Packages set up

We have set up three different folders to put packages, based on their needs, each of which can include new packages. Here is what you have to start:

```
- packages/
    - types
- services/
    - customers-api
- web/
    - customer-dash
```

To determine where to place a new package:

- If it is designed to be consumed by other packages, and not run on its own, put it in the `/packages` folder. These may be published to npm, but can also include private packages.
- If it is a user-facing app or website, it should live in the `/web` folder
- If it is a back-end service or node app, it should live in the `/services` folder

### Tools we set up

- [Yarn Workspaces](https://legacy.yarnpkg.com/en/docs/workspaces/)
- [Lernajs](https://github.com/lerna/lerna#readme)
- [Babel](https://babeljs.io/)
- [Jest](https://jestjs.io/)
- [Eslint](https://eslint.org/)

Each of these tools have configuration specific to their usage in a monorepo, which has been configured for you.

### Before running

Before trying to run the example, please unzip the provided .env files and place them in their respective locations and rename them to ```.env```

Ex: move _.env-customers-api_ to "/services/customers-api" & rename to _.env_

Ex: move _.env-customers-dash_ to "/web/customers-dash" & rename to _.env_


### Installing

At the root of the repository run yarn to install all dependencies.

Using yarn:

```bash
yarn install
```

After downloading all dependencies and providing the .env files to each respective service & app. You should be ready to run the application in development mode by running the following command in the root of our repository.

```bash
yarn run start:dev
```