import React, { useState } from 'react';
import { Redirect } from 'react-router-dom';
import { Card, CardBody, CardGroup, Col, Container, Form, Row, Alert } from 'reactstrap';

import { AuthConsumer } from '~/context';
import { ButtonLoader } from '~/components';
import logo from '~/assets/img/logo-compact.png';
import { useAuth0 } from '@auth0/auth0-react';

interface Props {}
const Login: React.FC<Props> = () => {
	const [isSubmitting, setIsSubmitting] = useState(false);
	const { loginWithRedirect } = useAuth0();

	return (
		<AuthConsumer>
			{({ login, isLogginIn, isAuth, errorMessage }) =>
				isAuth ? (
					<Redirect to="/" />
				) : (
					<div className="app flex-row align-items-center">
						<Container>
							<Row className="justify-content-center">
								<Col md="8">
									<CardGroup>
										<Card className="p-4">
											<CardBody>
												<div>
													<div className="row">
														<div className="col-6">
															<h3>Login</h3>
														</div>
														<div className="col-6 d-flex justify-content-end">
															<img height="64" width="64" src={logo}></img>
														</div>
													</div>
													{errorMessage && console.log(errorMessage)}
													{errorMessage ? <Alert color="danger">{errorMessage}</Alert> : null}
													<p className="text-muted">Access your account</p>
													<div className="justify-content-end float-right">
														<ButtonLoader
															small
															pullRight
															type="submit"
															color="primary"
															spinColor="#fff"
															loading={isSubmitting}
															disabled={isSubmitting}
															onClick={() => loginWithRedirect()}
														>
															Login
														</ButtonLoader>
													</div>
												</div>
											</CardBody>
										</Card>
										<Card className="text-white bg-dark py-5 d-md-down-none" style={{ width: '44%' }}>
											<CardBody className="text-center">
												<div>
													<h2>Sign-up</h2>
													<p className="">
														Lorem ipsum dolor sit amet consectetur adipisicing elit. Est consequuntur
														obcaecati laborum veritatis aperiam fugit, nobis similique magni minus ipsum?
													</p>

													<ButtonLoader
														small
														pullRight
														type="submit"
														color="primary"
														spinColor="#fff"
														loading={isSubmitting}
														disabled={isSubmitting}
														onClick={() =>
															loginWithRedirect({
																screen_hint: 'signup'
															})
														}
													>
														Sign-up
													</ButtonLoader>
												</div>
											</CardBody>
										</Card>
									</CardGroup>
								</Col>
							</Row>
						</Container>
					</div>
				)
			}
		</AuthConsumer>
	);
};

export default Login;
