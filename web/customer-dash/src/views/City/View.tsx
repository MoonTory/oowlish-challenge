import React, { useContext, useEffect, useState } from 'react';
import { useHistory } from 'react-router-dom';
import { Card, Row, Col, CardTitle, CardText } from 'reactstrap';

import { CustomerContext } from '~/context';

interface Props {}
const View: React.FC<Props> = () => {
	const history = useHistory();
	const [isMounted, setIsMounted] = useState(false);
	const { fetchCities, cities } = useContext(CustomerContext);

	useEffect(() => {
		(async () => {
			if (!isMounted) await fetchCities();

			setIsMounted(true);
		})();
	}, [cities]);

	return (
		<div className="animated fadeIn">
			<Row>
				{cities.map(city => (
					<Col key={city.city} lg={3} md={3} sm={6}>
						<Card
							body
							color="default"
							style={{ cursor: 'pointer' }}
							onClick={() => history.push({ pathname: `/cities/${city.city}/customers` })}
						>
							<CardTitle tag="h5">{city.city}</CardTitle>
							<CardText>Total customers: {city.customers_total}</CardText>
						</Card>
					</Col>
				))}
			</Row>
		</div>
	);
};

export default View;
