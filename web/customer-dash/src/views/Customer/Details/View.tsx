import { useParams } from 'react-router-dom';
import { ICustomerEntity } from '@oowlish-challenge/types';
import React, { useContext, useEffect, useState } from 'react';
import {
	Card,
	Row,
	Col,
	CardBody,
	CardHeader,
	Input,
	InputGroup,
	InputGroupAddon,
	InputGroupText
} from 'reactstrap';
// import { MapContainer, TileLayer, Marker, Popup } from 'react-leaflet';

import GoogleMapReact from 'google-map-react';

import { CustomerContext } from '~/context';
import { Spinners, FormInputs } from '~/components';

const AnyReactComponent: React.FC<{ text: string; lat: number; lng: number }> = ({ text }) => (
	<div
		style={{
			color: 'white',
			background: 'grey',
			padding: '5px 2.5px',
			display: 'inline-flex',
			textAlign: 'center',
			alignItems: 'center',
			justifyContent: 'center',
			borderRadius: '100%',
			transform: 'translate(-50%, -50%)'
		}}
	>
		{text}
	</div>
);
interface Props {}
const View: React.FC<Props> = () => {
	const { id } = useParams<{ id: string }>();
	const [isMounted, setIsMounted] = useState(false);
	const [customer, setCustomer] = useState<ICustomerEntity>(null);
	const { isFetching, findById } = useContext(CustomerContext);

	useEffect(() => {
		(async () => {
			if (!isMounted) setCustomer(await findById(id));

			setIsMounted(true);
		})();
	}, [customer]);

	return (
		<div className="animated fadeIn">
			<Row>
				<Col md={12}>
					<Card>
						<CardHeader>
							<span>
								<strong className="pr-2">Customer Details</strong>
							</span>
						</CardHeader>
						<CardBody>
							<div className="container-fluid" style={{ minHeight: '70vh', padding: '0' }}>
								{isFetching || customer === null ? (
									<Spinners.Ripple />
								) : (
									<>
										<Row>
											<Col md={12} style={{ padding: '1rem' }}>
												<FormInputs
													nCols={['col-md-4', 'col-md-4', 'col-md-4']}
													properties={[
														{
															label: 'Full name',
															type: 'text',
															placeholder: 'Full name',
															value: `${customer.last_name}, ${customer.first_name}`,
															name: 'fullName',
															disabled: true
														},
														{
															label: 'E-mail',
															type: 'text',
															placeholder: 'E-mail',
															value: customer.email,
															name: 'email',
															disabled: true
														},
														{
															label: 'Title',
															type: 'text',
															placeholder: 'Title',
															value: customer.title,
															name: 'title',
															disabled: true
														}
													]}
												/>
												<FormInputs
													nCols={['col-md-4', 'col-md-4', 'col-md-4']}
													properties={[
														{
															label: 'Company',
															type: 'text',
															placeholder: 'Company',
															value: customer.company,
															name: 'company',
															disabled: true
														},
														{
															label: 'City',
															type: 'text',
															placeholder: 'City',
															value: customer.city,
															name: 'city',
															disabled: true
														},
														{
															label: 'Gender',
															type: 'text',
															placeholder: 'Gender',
															value: customer.gender,
															name: 'gender',
															disabled: true
														}
													]}
												/>
											</Col>
										</Row>
										<br />
										<Row>
											<Col md={2}></Col>
											<Col md={8}>
												<div style={{ height: '65vh', width: '100%' }}>
													<GoogleMapReact
														bootstrapURLKeys={{ key: process.env.GOOGLE_MAPS_API_KEY }}
														defaultCenter={{ lat: customer.lat, lng: customer.long }}
														defaultZoom={14}
													>
														<AnyReactComponent lat={customer.lat} lng={customer.long} text="Here" />
													</GoogleMapReact>
												</div>
											</Col>
											<Col md={2}></Col>
										</Row>
									</>
								)}
							</div>
						</CardBody>
					</Card>
				</Col>
			</Row>
		</div>
	);
};

export default View;
