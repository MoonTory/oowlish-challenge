import { useParams } from 'react-router-dom';
import React, { useContext, useEffect, useState } from 'react';
import { Row, Col, Card, CardHeader, CardBody } from 'reactstrap';

import { Table } from './Table';
import { filterCustomerData } from './Filter';

import { CustomerContext } from '~/context';

interface Props {}
const List: React.FC<Props> = () => {
	const { city } = useParams<{ city: string }>();
	const [isMounted, setIsMounted] = useState(false);
	const {
		fetchCustomers,
		customers,
		pagination,
		isFetching,
		resetPagination,
		handleChangePage,
		handleChangeRowsPerPage
	} = useContext(CustomerContext);

	const { itemsPerPage, page, totalItems } = pagination;

	useEffect(() => {
		(async () => {
			if (!isMounted) await fetchCustomers({ page, limit: itemsPerPage }, city);

			setIsMounted(true);

			return () => {
				resetPagination();
			};
		})();
	}, [customers]);

	const onChangePage = (page: number, totalRows: number) => handleChangePage(page, totalRows, city);

	return (
		<div className="animated fadeIn">
			<Row>
				<Col md={12}>
					<Card>
						<CardHeader>
							<span>
								<strong className="pr-2">Customers</strong>
							</span>
						</CardHeader>
						<CardBody>
							<div className="container-fluid" style={{ minHeight: '70vh', padding: '0' }}>
								<Row>
									<Col md={12} style={{ padding: '0' }}>
										<Table
											filter={filterCustomerData}
											tableData={customers}
											isLoading={isFetching}
											paginationTotalRows={totalItems}
											handleChangePage={onChangePage}
											handleChangeRowsPerPage={handleChangeRowsPerPage}
										/>
									</Col>
								</Row>
							</div>
						</CardBody>
					</Card>
				</Col>
			</Row>
		</div>
	);
};

export default List;
