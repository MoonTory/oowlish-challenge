import React from 'react';
import { ICustomerEntity } from '@oowlish-challenge/types';
import { Input, InputGroup, InputGroupAddon, InputGroupText } from 'reactstrap';

import { removeDiacritics } from '~/utils';

export function filterCustomerData(collection: ICustomerEntity[], filterText: string) {
	return collection.filter(item => {
		return (
			removeDiacritics(item.first_name.toLowerCase() + ' ' + item.last_name.toLowerCase())
				.toLowerCase()
				.indexOf(filterText.toLowerCase()) !== -1
		);
	});
}

interface Props {
	style?: any;
	filterText: string;
	onClear: () => void;
	onChange: (e: React.ChangeEvent<HTMLInputElement>) => void;
}

export const Filter: React.FC<Props> = ({ onChange, filterText, onClear, style }) => {
	return (
		<div style={style}>
			<InputGroup>
				<InputGroupAddon addonType="prepend">
					<InputGroupText>
						<i style={{ color: 'gray' }} className="fas fa-search" />
					</InputGroupText>
				</InputGroupAddon>
				<Input
					bsSize="sm"
					type="text"
					value={filterText}
					placeholder="Search for Customers"
					onChange={onChange}
				/>
				<InputGroupAddon addonType="append">
					<InputGroupText>
						<i style={{ color: 'gray', cursor: 'pointer' }} className="fas fa-times" onClick={onClear} />
					</InputGroupText>
				</InputGroupAddon>
			</InputGroup>
		</div>
	);
};
