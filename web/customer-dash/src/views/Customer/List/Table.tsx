import React, { useMemo } from 'react';
import { useHistory } from 'react-router-dom';
import DataTable from 'react-data-table-component';
import { ICustomerEntity } from '@oowlish-challenge/types';

import { Filter } from './Filter';

import { Spinners, CustomButton } from '~/components';

interface Props {
	isLoading: boolean;
	tableData: ICustomerEntity[];
	paginationTotalRows: number;
	filter: (data: any[], filterText: string) => any | void;
	handleChangePage: (page: number, totalRows: number) => any | void;
	handleChangeRowsPerPage: (currentRowsPerPage: number, currentPage: number) => any | void;
}

export const Table: React.FC<Props> = ({
	tableData,
	isLoading,
	filter,
	handleChangePage,
	paginationTotalRows,
	handleChangeRowsPerPage
}) => {
	const history = useHistory();
	const [filterText, setFilterText] = React.useState('');
	const [resetPaginationToggle, setResetPaginationToggle] = React.useState(false);
	const filteredItems = filter(tableData, filterText);

	const subHeaderComponentMemo = React.useMemo(() => {
		const handleClear = () => {
			if (filterText) {
				setResetPaginationToggle(!resetPaginationToggle);
				setFilterText('');
			}
		};

		return (
			<React.Fragment>
				<Filter
					style={{ padding: '0' }}
					onChange={e => setFilterText(e.target.value)}
					onClear={handleClear}
					filterText={filterText}
				/>
			</React.Fragment>
		);
	}, [filterText, resetPaginationToggle]);

	const columns = useMemo(
		() => [
			{
				name: 'ID',
				selector: 'id',
				sortable: true
			},
			{
				name: 'Name',
				selector: '',
				cell: (row: any) => (
					<>
						{row.last_name}, {row.first_name}
					</>
				),
				sortable: true
			},
			{
				name: 'E-mail',
				selector: 'email',
				sortable: true
			},
			{
				name: 'Title',
				selector: 'title',
				sortable: true
			},
			{
				name: 'Company',
				selector: 'company',
				sortable: true
			},
			{
				name: 'Gender',
				selector: 'gender',
				sortable: true
			},
			{
				name: '',
				selector: '',
				cell: (row: any) => (
					<CustomButton
						onClick={() => history.push({ pathname: `/customers/${row.id}` })}
						style={{ float: 'right' }}
						small
						pullRight
						type="submit"
						color="secondary"
					>
						Details
					</CustomButton>
				),
				ignoreRowClick: true,
				allowOverflow: true,
				button: true
			}
		],
		[]
	);

	return (
		<React.Fragment>
			<DataTable
				columns={columns}
				data={filteredItems}
				defaultSortField="checkoutDate"
				pagination
				paginationServer
				onChangePage={handleChangePage}
				onChangeRowsPerPage={handleChangeRowsPerPage}
				noHeader={true}
				highlightOnHover={true}
				subHeader
				subHeaderComponent={subHeaderComponentMemo}
				paginationTotalRows={paginationTotalRows}
				paginationResetDefaultPage={resetPaginationToggle} // optionally, a hook to reset pagination to page 1
				progressPending={isLoading}
				progressComponent={<Spinners.Ripple />}
			/>
		</React.Fragment>
	);
};
