import React from 'react';
import { useAuth0, withAuthenticationRequired } from '@auth0/auth0-react';
import { Route, Redirect, RouteProps } from 'react-router-dom';

import { Preloader } from '~/components';

export const ProtectedRoute: React.FC<RouteProps> = ({ component, ...rest }) => {
	const { isAuthenticated, isLoading } = useAuth0();

	if (isLoading) {
		return <Preloader />;
	}

	return (
		<>
			{isAuthenticated ? (
				<Route
					component={withAuthenticationRequired(component as any, {
						onRedirecting: () => <Preloader />
					})}
					{...rest}
				/>
			) : (
				<Redirect to="/login" />
			)}
		</>
	);
};
