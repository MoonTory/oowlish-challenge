import React from 'react';

const Home = React.lazy(() => import('~/views/Home'));
const City = React.lazy(() => import('~/views/City'));
const CustomersList = React.lazy(() => import('~/views/Customer/List'));
const CustomersDetails = React.lazy(() => import('~/views/Customer/Details'));

const routes = [
	{ path: '/home', exact: true, component: Home },
	{ path: '/cities', exact: true, name: 'Cities', component: City },
	{ path: '/cities/:city/customers', exact: true, name: 'Customers', component: CustomersList },
	{ path: '/customers/:id', exact: true, name: 'Customer Details', component: CustomersDetails }
];

export default routes;
