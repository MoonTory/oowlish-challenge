import axios, { AxiosRequestConfig } from 'axios';

export function withAuth(token: string): AxiosRequestConfig {
	return {
		headers: {
			Authorization: 'Bearer ' + token
		}
	};
}

export const Axios = axios.create({
	baseURL: 'http://localhost:5001'
});
