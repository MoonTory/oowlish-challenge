export const currentDate = () => {
	const today = new Date();

	const day = today.getDate();

	const month = (today.getMonth() + 1).toString().padStart(2, '0');

	const year = today.getFullYear();

	return { day, month, year };
};
