import React, { useState, useEffect } from 'react';

// import { Axios, withAuth } from '~/lib';
import { AuthContext } from './Context';

const localToken = localStorage.getItem('LOCAL_TOKEN');
const localAvatar = localStorage.getItem('LOCAL_AVATAR');
const localRefreshToken = localStorage.getItem('LOCAL_REFRESH_TOKEN');

export interface Props {
	children: React.ReactNode;
}

export const AuthProvider: React.FC<Props> = ({ children }) => {
	const [user, setUser] = useState(undefined);
	const [isLogginIn, setIsLogginIn] = useState(false);
	const [errorMessage, setErrorMessage] = useState('');
	const [isAuth, setIsAuth] = useState(localToken ? true : false);
	const [dark, setDark] = useState(false);
	const [avatar, setAvatar] = useState(localAvatar ? localAvatar : '');
	const [token, setToken] = useState(localToken ? localToken : undefined);
	const [refreshToken, setRefreshToken] = useState(localRefreshToken ? localRefreshToken : undefined);

	useEffect(() => {
		(async () => {})();
	}, []);

	const login = async (_: string, __: string) => {
		try {
			setIsLogginIn(true);

			setIsAuth(true);
			setIsLogginIn(false);
			setErrorMessage(undefined);
		} catch (error) {
			setIsAuth(false);
			setIsLogginIn(false);
			setErrorMessage(error.response.data.error.message);
		}
	};

	const logout = async () => {
		localStorage.removeItem('LOCAL_TOKEN');
		localStorage.removeItem('LOCAL_REFRESH_TOKEN');
		setIsAuth(false);
		setUser(undefined);
		setToken(undefined);
		setRefreshToken(undefined);
	};

	return (
		<AuthContext.Provider
			value={{
				token: token,
				refreshToken: refreshToken,
				avatar: avatar,
				user: user,
				isAuth: isAuth,
				isLogginIn: isLogginIn,
				dark: dark,
				errorMessage: errorMessage,
				login: login,
				logout: logout,
				setDark: setDark
			}}
		>
			{children}
		</AuthContext.Provider>
	);
};
