import React from 'react';

interface AuthState {
	avatar: string;
	token?: string;
	refreshToken?: string;
	errorMessage?: string;
	user?: any;
	isAuth: boolean;
	dark: boolean;
	isLogginIn: boolean;
	setDark: (dark: any) => void;
	login: (email: string, password: string) => void;
	logout: () => void;
}

const initialState: AuthState = {
	token: undefined,
	refreshToken: undefined,
	errorMessage: undefined,
	avatar: '',
	user: undefined,
	isAuth: false,
	dark: false,
	isLogginIn: false,
	setDark: (dark: boolean) => {},
	login: () => {},
	logout: () => {}
};

const AuthContext = React.createContext<AuthState>(initialState);

const AuthConsumer = AuthContext.Consumer;

export { AuthContext, AuthState, AuthConsumer };
