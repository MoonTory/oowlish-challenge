import qs from 'qs';
import React from 'react';
import { ICustomerEntity, ITotalCustomersByCityView } from '@oowlish-challenge/types';

import { Axios } from '~/lib';
import { CustomerContext, PaginationState } from './Context';

interface Props {
	children: React.ReactNode;
}

export const CustomerProvder: React.FC<Props> = ({ children }) => {
	const [customers, setCustomers] = React.useState<ICustomerEntity[]>([]);
	const [cities, setCities] = React.useState<ITotalCustomersByCityView[]>([]);
	const [isFetching, setIsFetching] = React.useState(false);
	const [pagination, setPagination] = React.useState<PaginationState>({
		page: 1,
		totalPages: 1,
		totalItems: 0,
		itemsPerPage: 10
	});

	const fetchCustomers = async (
		options: { page?: number; limit?: number; offset?: number },
		city?: string
	) => {
		try {
			setIsFetching(true);
			const query = qs.stringify({
				page: options.page ? options.page : pagination.page,
				limit: options.limit ? options.limit : pagination.itemsPerPage,
				city
			});

			const { data } = await Axios.get(`/v1/customers${query ? `?${query}` : ''}`);

			setCustomers(data.data.docs);
			setPagination({
				...pagination,
				totalItems: data.data.totalDocs,
				page: data.data.page,
				totalPages: data.data.totalPages
			});
			setIsFetching(false);
			return data.data.docs;
		} catch (error) {
			console.log('error', error);
			setCustomers([]);
			setIsFetching(false);
			return [];
		}
	};

	const fetchCities = async () => {
		try {
			setIsFetching(true);

			const { data } = await Axios.get(`/v1/customers/city`);

			setCities(data.data);
			setIsFetching(false);
			return data.data;
		} catch (error) {
			setCities([]);
			setIsFetching(false);
			return [];
		}
	};

	const findById = async (id: string) => {
		try {
			setIsFetching(true);

			const { data } = await Axios.get(`/v1/customers/${id}`);

			setIsFetching(false);
			return data.data;
		} catch (error) {
			setIsFetching(false);
			return null;
		}
	};

	const resetPagination = () => {
		setPagination({
			...pagination,
			totalItems: 0,
			page: 1,
			totalPages: 0
		});
	};

	const handleChangePage = async (page: number, _: number, city?: string) => {
		setPagination({ ...pagination, page: page });
		await fetchCustomers({ page, limit: pagination.itemsPerPage }, city);
	};

	const handleChangeRowsPerPage = async (currentRowsPerPage: number, currentPage: number) => {
		setPagination({ ...pagination, itemsPerPage: currentRowsPerPage });
		await fetchCustomers({ page: currentPage, limit: currentRowsPerPage });
	};

	return (
		<CustomerContext.Provider
			value={{
				resetPagination,
				pagination,
				isFetching,
				cities,
				customers,
				findById,
				fetchCustomers,
				fetchCities,
				handleChangePage,
				handleChangeRowsPerPage
			}}
		>
			{children}
		</CustomerContext.Provider>
	);
};
