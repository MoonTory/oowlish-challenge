import React from 'react';
import { ICustomerEntity, ITotalCustomersByCityView } from '@oowlish-challenge/types';

export interface PaginationState {
	page: number;
	totalPages: number;
	totalItems: number;
	itemsPerPage: number;
}

interface CustomerState {
	isFetching: boolean;
	pagination: PaginationState;
	customers: ICustomerEntity[];
	cities: ITotalCustomersByCityView[];
	findById: (id: string) => Promise<ICustomerEntity | any>;
	fetchCities: () => Promise<ITotalCustomersByCityView[] | any>;
	fetchCustomers: (
		query: { page?: number; limit?: number; offset?: number },
		city?: string
	) => Promise<ICustomerEntity[] | any>;
	resetPagination: () => any | void;
	handleChangePage: (page: number, totalRows: number, city?: string) => any | void;
	handleChangeRowsPerPage: (currentRowsPerPage: number, currentPage: number) => any | void;
}

const initialState: CustomerState = {
	isFetching: false,
	pagination: {
		page: 1,
		totalPages: 1,
		totalItems: 0,
		itemsPerPage: 10
	},
	cities: [],
	customers: [],
	fetchCities: async () => {},
	fetchCustomers: async () => {},
	findById: async (id: string) => {},
	resetPagination: () => {},
	handleChangePage: (page: number, totalRows: number) => {},
	handleChangeRowsPerPage: (currentRowsPerPage: number, currentPage: number) => {}
};

const CustomerContext = React.createContext<CustomerState>(initialState);

const CustomerConsumer = CustomerContext.Consumer;

export { CustomerContext, CustomerState, CustomerConsumer };
