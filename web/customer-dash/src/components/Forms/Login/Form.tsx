import React, { useState } from 'react';
import { Col, Input, InputGroup, InputGroupAddon, InputGroupText, Row } from 'reactstrap';

import { ButtonLoader } from '~/components';

interface Props {
	isLoggingIn: boolean;
	onSubmit: (values: any) => void;
}

export const Form: React.FC<Props> = ({ isLoggingIn, onSubmit }) => {
	const [username, setUsername] = useState<string>('');
	const [password, setPassword] = useState<string>('');

	const handleClick = () => {
		onSubmit({ username, password });
	};

	return (
		<>
			<InputGroup className="mb-3">
				<InputGroupAddon addonType="prepend">
					<InputGroupText>
						<i className="icon-user"></i>
					</InputGroupText>
				</InputGroupAddon>
				<Input
					bsSize="sm"
					type="text"
					placeholder="Username / E-mail"
					autoComplete="username"
					onChange={(e: React.FormEvent<HTMLInputElement>) => setUsername(e.currentTarget.value)}
				/>
			</InputGroup>
			<InputGroup className="mb-4">
				<InputGroupAddon addonType="prepend">
					<InputGroupText>
						<i className="icon-lock"></i>
					</InputGroupText>
				</InputGroupAddon>
				<Input
					bsSize="sm"
					type="password"
					placeholder="Password"
					autoComplete="current-password"
					onChange={(e: React.FormEvent<HTMLInputElement>) => setPassword(e.currentTarget.value)}
				/>
			</InputGroup>
			<Row>
				<Col xs="6">
					<ButtonLoader
						small
						pullRight
						type="submit"
						color="primary"
						spinColor="#fff"
						loading={isLoggingIn}
						disabled={isLoggingIn}
						onClick={handleClick}
					>
						Login
					</ButtonLoader>
				</Col>
			</Row>
		</>
	);
};
