import React from 'react';
import { Modal, ModalBody, Alert, ModalFooter, Button } from 'reactstrap';
import { Link } from 'react-router-dom';

const AlertModal = ({ isOpen, toggle, closeAll, togglePattern, children, toggleAll, alert, description }) => {
	return (
		<Modal isOpen={isOpen} toggle={toggle} onClosed={closeAll ? togglePattern : undefined}>
			<ModalBody>
				<Alert style={{ textAlign: 'center' }} color={alert}>
					{children}
				</Alert>
			</ModalBody>
			<ModalFooter>
				{description === 'noAdmin' ? (
					<Link to={'/'}>
						<Button color="dark" onClick={toggle}>
							Ok
						</Button>
					</Link>
				) : (
					<Link to={'/accountmanager'}>
						<Button color="dark" onClick={toggleAll ? toggleAll : toggle}>
							Ok
						</Button>
					</Link>
				)}
			</ModalFooter>
		</Modal>
	);
};

export default AlertModal;
