import React, { Fragment, useContext } from 'react';
import {
	Badge,
	UncontrolledDropdown,
	DropdownItem,
	DropdownMenu,
	DropdownToggle,
	Nav,
	Button
} from 'reactstrap';

import { AppNavbarBrand, AppSidebarToggler } from '@coreui/react';
import logoDark from '~/assets/img/logo-full-dark.png';
import logoLight from '~/assets/img/logo-full-light.png';
import sygnet from '~/assets/img/logo-compact.png';
import defaultAvatar from '~/assets/img/default-avatar.png';

import { AuthConsumer, AuthContext } from '~/context';

import { FiSun, FiMoon } from 'react-icons/fi';
import { style } from '~/notification';
import { useAuth0 } from '@auth0/auth0-react';

interface Props {}
const DefaultHeader: React.FC<Props> = () => {
	const { setDark, dark } = useContext(AuthContext);
	const { logout, user } = useAuth0();

	if (dark) {
		const anchor = document.getElementsByClassName('navbar-brand')[0] as HTMLAnchorElement;
		(anchor.style as any) = { ...style, backgroundColor: 'rgb(38, 42, 46)' };
	}

	return (
		<AuthConsumer>
			{({ avatar }) => (
				<Fragment>
					<AppSidebarToggler className="d-lg-none" display="md" mobile />
					<AppNavbarBrand
						full={{ src: dark ? logoLight : logoDark, width: 120, height: 30, alt: 'CoreUI Logo' }}
						minimized={{ src: sygnet, width: 30, height: 30, alt: 'CoreUI Logo' }}
						onClick={() => console.log('Fire')}
						style={{ cursor: 'pointer', background: '#fff' }}
					/>
					<AppSidebarToggler className="d-md-down-none" display="lg" />

					<Nav className="ml-auto" navbar>
						{dark ? (
							<FiSun
								style={{ cursor: 'pointer', marginRight: '20px', color: '#fff' }}
								size={20}
								onClick={() => setDark(!dark)}
							/>
						) : (
							<FiMoon
								style={{ cursor: 'pointer', marginRight: '20px' }}
								size={20}
								onClick={() => setDark(!dark)}
							/>
						)}

						<UncontrolledDropdown nav direction="down">
							<DropdownToggle nav>
								<img src={user.picture} className="img-avatar" alt="profile picture" />
							</DropdownToggle>
							<DropdownMenu right>
								<DropdownItem header tag="div" className="text-center">
									<strong>Account</strong>
								</DropdownItem>
								{/* <DropdownItem>
										<i className="fa fa-bell"></i> Updates<Badge color="info">42</Badge>
									</DropdownItem>
									<DropdownItem>
										<i className="fa fa-envelope"></i> Messages<Badge color="success">42</Badge>
									</DropdownItem>
									<DropdownItem>
										<i className="fa fa-tasks"></i> Tasks<Badge color="danger">42</Badge>
									</DropdownItem>
									<DropdownItem>
										<i className="fa fa-comments"></i> Comments<Badge color="warning">42</Badge>
									</DropdownItem>
									<DropdownItem header tag="div" className="text-center">
										<strong>Settings</strong>
									</DropdownItem>
									<DropdownItem>
										<i className="fa fa-user"></i> Profile
									</DropdownItem>
									<DropdownItem>
										<i className="fa fa-wrench"></i> Settings
									</DropdownItem>
									<DropdownItem>
										<i className="fas fa-dollar-sign"></i> Payments<Badge color="secondary">42</Badge>
									</DropdownItem>
									<DropdownItem>
										<i className="fa fa-file"></i> Projects<Badge color="primary">42</Badge>
									</DropdownItem>
									<DropdownItem divider />
									<DropdownItem>
										<i className="fas fa-shield-alt"></i> Lock Account
									</DropdownItem> */}
								<DropdownItem
									onClick={() =>
										logout({
											returnTo: window.location.origin
										})
									}
								>
									<i className="fa fa-lock"></i> Logout
								</DropdownItem>
							</DropdownMenu>
						</UncontrolledDropdown>
					</Nav>
					{/* <AppAsideToggler className="d-md-down-none" />
						<AppAsideToggler className="d-lg-none" mobile /> */}
				</Fragment>
			)}
		</AuthConsumer>
	);
};

export default DefaultHeader;
