import React, { Suspense, useContext } from 'react';
import { Redirect, Route, Switch, RouteComponentProps } from 'react-router-dom';
import * as router from 'react-router-dom';
import { Container } from 'reactstrap';

import {
	AppAside,
	AppFooter,
	AppHeader,
	AppSidebar,
	AppSidebarFooter,
	AppSidebarForm,
	AppSidebarHeader,
	AppSidebarMinimizer,
	AppBreadcrumb2 as AppBreadcrumb,
	AppSidebarNav2 as AppSidebarNav
} from '@coreui/react';

import { Preloader } from '~/components';
// routes config &  sidebar nav config
import { routes, navigation } from '~/routes';
import { AuthContext } from '~/context';

const DefaultFooter = React.lazy(() => import('./DefaultFooter'));
const DefaultHeader = React.lazy(() => import('./DefaultHeader'));

interface Props extends RouteComponentProps {}
const DefaultLayout: React.FC<Props> = props => {
	const loading = () => <i className="fa fa-spinner fa-spin" />;
	const { dark } = useContext(AuthContext);

	const header = document.querySelector('.breadcrumb');

	if (header !== null) {
		if (dark) {
			header.classList.add('breadcrumb-dark');
		} else {
			header.classList.remove('breadcrumb-dark');
		}
	}

	return (
		<div className="app">
			<AppHeader style={{ background: dark ? '#262a2e' : null, borderBottom: dark ? 'none' : null }} fixed>
				<Suspense fallback={loading()}>
					<DefaultHeader />
				</Suspense>
			</AppHeader>
			<div className="app-body" style={{ background: dark ? '#0a0b18' : null, fontSize: '100%' }}>
				<AppSidebar style={{}} fixed display="lg">
					<AppSidebarHeader />
					<AppSidebarForm />
					<Suspense fallback={loading()}>
						<AppSidebarNav navConfig={navigation} {...props} router={router} />
					</Suspense>
					<AppSidebarFooter />
					<AppSidebarMinimizer />
				</AppSidebar>
				<main className="main">
					<AppBreadcrumb appRoutes={routes} router={router} />
					<Container style={{ padding: 10, background: dark ? '#0a0b18' : null }} fluid>
						<Suspense fallback={<Preloader />}>
							<Switch>
								{routes.map((route: any, idx) => {
									return route.component ? (
										<Route
											key={idx}
											path={route.path}
											exact={route.exact}
											render={props => <route.component {...props} />}
										/>
									) : null;
								})}
								<Redirect from="/" to="/cities" />
							</Switch>
						</Suspense>
					</Container>
				</main>
				{/* <AppAside fixed>
					<Suspense fallback={loading()}>
						<DefaultAside />
					</Suspense>
				</AppAside> */}
			</div>
			<AppFooter
				style={{
					background: dark ? '#262a2e' : null,
					color: dark ? '#dadad5' : null,
					borderTop: dark ? 'none' : null
				}}
			>
				<Suspense fallback={loading()}>
					<DefaultFooter />
				</Suspense>
			</AppFooter>
		</div>
	);
};

export default DefaultLayout;
