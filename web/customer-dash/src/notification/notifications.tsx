import React from 'react';
import { Map } from '@ctci/common';

import { Notification } from './interfaces';

interface NotificationsMap extends Map<Notification> {}

export const messages = {
	error: (message: string): Notification => {
		return {
			title: <span data-notify="icon" className="fas fa-exclamation-triangle" />,
			message: <strong>{message}</strong>,
			level: 'error',
			position: 'tc',
			autoDismiss: 5
		};
	}
};
