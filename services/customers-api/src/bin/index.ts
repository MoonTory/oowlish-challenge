import { App } from '../app';
import { config } from '../config';

(async () => {
	const app = new App(config);
	await app.start();
})();
