export const TYPES = {
	CustomerMapper: Symbol('CustomerMapper'),
	CustomerBusiness: Symbol('CustomerBusiness'),
	CustomerRepository: Symbol('CustomerRepository')
};
