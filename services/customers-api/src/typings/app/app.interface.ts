import { IContainer } from '../container';
import { AbstractDatabaseAdapter } from '../modules';

export interface IApplication {
	start: () => Promise<void>;
}

export abstract class AbstractApplication<Config> implements IApplication {
	protected _db: AbstractDatabaseAdapter<Config>;
	protected readonly _config: Config;

	public get config() {
		return this._config;
	}

	protected constructor(config: Config) {
		this._config = config;
	}
	public abstract start: () => Promise<void>;
}
