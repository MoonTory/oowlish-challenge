export interface IConfig {
	API_VERSION: string | number;
	APP_PORT: string | number;
	NODE_ENV: string;
	DB_CONNECTION_STRING: string;
	IN_MEMORY_DB: boolean;
	GOOGLE_MAPS_API_KEY: string;
}
