export interface IServer {
	listen(): Promise<void>;
}

export abstract class AbstractServer<Config> implements IServer {
	protected readonly _config: Config;

	protected constructor(config: Config) {
		this._config = config;
	}
	abstract listen(): Promise<void>;
	protected abstract config(): Promise<void>;
}
