import { Map } from '@oowlish-challenge/types';
import { EndpointFunction } from './function';

export type EndpointMap = Map<EndpointFunction>;

export * from './function';
