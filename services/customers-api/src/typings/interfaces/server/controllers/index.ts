import { Router } from 'express';

export abstract class IController<Config> {
	protected _path: string;
	protected _config: Config;
	public router: Router;

	constructor(path: string, config: Config) {
		this._path = '/' + path;
		this._config = config;
		this.router = Router();
	}
}
