import { IWrite, IRead } from '@oowlish-challenge/types';

export interface IBusiness<T> extends Partial<IRead<T>>, Partial<IWrite<T>> {}
