import { ICustomerEntity, ITotalCustomersByCityView, OperationResult } from '@oowlish-challenge/types';
import { IConfig } from '../../config';
import { PaginateResult } from '../repo';
import { IBusiness } from './business';

export abstract class ICustomerBusiness implements IBusiness<ICustomerEntity> {
	protected readonly _config: IConfig;

	protected constructor(config: IConfig) {
		this._config = config;
	}

	abstract create: (item: ICustomerEntity) => Promise<OperationResult<ICustomerEntity>>;

	abstract findById: (id: string) => Promise<OperationResult<ICustomerEntity | null>>;

	abstract totalCustomersByCity: (city: string) => Promise<OperationResult<ITotalCustomersByCityView>>;

	abstract listCities: () => Promise<OperationResult<ITotalCustomersByCityView[]>>;

	abstract retrieve: (
		query?: any,
		options?: { page?: number; limit?: number; offset?: number }
	) => Promise<OperationResult<PaginateResult<ICustomerEntity>>>;
}
