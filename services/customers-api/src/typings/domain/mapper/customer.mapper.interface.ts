import { ICustomerEntity } from '@oowlish-challenge/types';
import { ICustomerDTOEntity } from '../dto';
import { IMapper } from './mapper.interface';

export abstract class ICustomerMapper implements IMapper<ICustomerEntity, ICustomerDTOEntity> {
	toDTO: (data: ICustomerEntity) => ICustomerDTOEntity;
	toPersistence: (data: ICustomerEntity) => any;
	toDomain: (data: any) => ICustomerEntity;
}
