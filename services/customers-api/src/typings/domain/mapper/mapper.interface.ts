export interface IMapper<E, D> {
	toDTO: (data: E) => D;
	toPersistence: (data: E) => any;
	toDomain: (data: any) => E;
}
