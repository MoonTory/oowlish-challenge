export interface IEntityDTO {
	id?: string;
}

export abstract class EntityDTO {
	public readonly id?: string;

	constructor(id?: string) {
		this.id = id ? id : undefined;
	}
}
