import { ITimestamp } from '@oowlish-challenge/types';
import { IEntityDTO } from './entity';

export type ICustomerDTOEntity = IEntityDTO & ICustomerDTO;
export interface ICustomerDTO extends ITimestamp {
	first_name: string;
	last_name: string;
	email: string;
	gender: string;
	company: string;
	city: string;
	title: string;
	lat: number;
	long: number;
}
