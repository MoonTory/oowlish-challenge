import mongoose from 'mongoose';
import { IRepository } from '../repo.interface';

export interface PaginateResult<T> {
	docs: T[];
	totalDocs: number;
	limit: number;
	page?: number;
	totalPages: number;
	nextPage?: number | null;
	prevPage?: number | null;
	pagingCounter: number;
	hasPrevPage: boolean;
	hasNextPage: boolean;
	meta?: any;
	[customLabel: string]: T[] | number | boolean | null | undefined;
}

export abstract class MongooseRepository<T extends mongoose.Document> implements IRepository<T> {
	protected _model: mongoose.Model<T>;

	constructor(model: mongoose.Model<T>) {
		this._model = model;
	}

	abstract delete: (_id: string) => Promise<boolean>;
	abstract create: (item: any) => Promise<any>;
	abstract update: (query: any, item: any) => Promise<any>;
	abstract retrieve: (...args: any[]) => Promise<any>;
	abstract findById: (_id: string, ...args: any[]) => Promise<any | null>;
}
