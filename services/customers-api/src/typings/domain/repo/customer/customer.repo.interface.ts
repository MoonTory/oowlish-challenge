import mongoose, { PaginateModel } from 'mongoose';
import { ICustomerEntity } from '@oowlish-challenge/types';
import { MongooseRepository, PaginateResult } from '../mongo';
import { ICustomerModel } from '../../../../domain';

export abstract class ICustomerRepository extends MongooseRepository<ICustomerModel> {
	protected _model: PaginateModel<ICustomerModel>;

	constructor(model: PaginateModel<ICustomerModel>) {
		super(model);
	}

	abstract listByCity: (city: string) => Promise<ICustomerEntity[]>;
	abstract listCities: () => Promise<any[]>;
	abstract delete: (_id: string) => Promise<boolean>;
	abstract create: (item: any) => Promise<ICustomerEntity>;
	abstract update: (query: any, item: any) => Promise<ICustomerEntity>;
	abstract list: (query?: any) => Promise<ICustomerEntity[]>;
	abstract retrieve: (query?: any, paginate?: { page?: number; limit?: number; offset?: number }) => Promise<PaginateResult<ICustomerEntity>>;
	abstract findById: (_id: string, ...args: any[]) => Promise<ICustomerEntity | null>;
}
