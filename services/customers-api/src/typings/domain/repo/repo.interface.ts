import { IWrite, IRead } from '@oowlish-challenge/types';

export interface IRepository<T> extends IWrite<T>, IRead<T> {}
