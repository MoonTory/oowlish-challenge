import { IEntity } from '@oowlish-challenge/types';
import { v4 } from 'uuid';

export abstract class Entity implements IEntity {
	readonly id?: string;

	constructor(id?: string) {
		this.id = id ? id : v4();
	}
}

export const isEntity = (v: any): v is Entity => {
	return v instanceof Entity;
};
