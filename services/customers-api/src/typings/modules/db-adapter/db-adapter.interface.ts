export interface IDatabaseAdapter {
	connect(): Promise<void>;
}

export abstract class AbstractDatabaseAdapter<Config> implements IDatabaseAdapter {
	protected readonly _config: Config;

	constructor(config: Config) {
		this._config = config;
	}
	public abstract connect(): Promise<void>;
}
