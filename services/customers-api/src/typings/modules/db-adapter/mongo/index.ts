import { Mongoose } from 'mongoose';
import { IConfig } from '../../../config';
import { AbstractDatabaseAdapter } from '../db-adapter.interface';

export abstract class AbstractMongoDatabaseAdapter extends AbstractDatabaseAdapter<IConfig> {
	protected _connection: Mongoose;
	protected _connectionString: string;

	public get connection() {
		return this._connection;
	}
}
