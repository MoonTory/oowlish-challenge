export const {
	API_VERSION = process.env.API_VERSION || '',
	APP_PORT = process.env.PORT || 5000,
	NODE_ENV = process.env.NODE_ENV || '',
	// Mongo Config
	DB_CONNECTION_STRING = process.env.DB_CONNECTION_STRING || '',
	IN_MEMORY_DB = process.env.IN_MEMORY_DB || DB_CONNECTION_STRING === '' ? true : false,
	GOOGLE_MAPS_API_KEY = process.env.GOOGLE_MAPS_API_KEY || ''
} = process.env;
