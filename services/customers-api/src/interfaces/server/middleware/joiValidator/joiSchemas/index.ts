/* eslint-disable prettier/prettier */
import joi from 'joi';

export const JoiSchemas = {
	query: {
		paginate: {
			page: joi.object().keys({ page: joi.number() }),
			limit: joi.object().keys({ limit: joi.number() }),
			offset: joi.object().keys({ offset: joi.number() })
		},
		sort: {
			sortField: joi.object().keys({ sortField: joi.string() }),
			sortOrder: joi.object().keys({ sortOrder: joi.string() })
		},
		city: joi.object().keys({ city: joi.string() })
	}
};
