import { Schema, ValidationErrorItem } from 'joi';
import { Response, NextFunction } from 'express';

import { Middleware } from '../../../../typings';

type ReqProperty = 'params' | 'body' | 'query';
export const validate: Middleware = (schema: Schema, property: ReqProperty = 'body') => (req: any, res: Response, next: NextFunction) => {
	const { error, value } = schema.validate(req[property]);

	if (error) {
		const { details } = error;
		const message = details.map((i: ValidationErrorItem) => ({ field: i.context?.label, message: i.message }));

		return res.status(422).json({
			error: {
				code: `producer/${property}-validation-error`,
				messages: message
			}
		});
	}

	if (property === 'body') req.value = value;
	if (property === 'query') req.query = value;
	return next();
};

export const validateSchema = (schema: Schema, input: any) => {
	const { error, value } = schema.validate(input);

	if (error) {
		const { details } = error;
		const message = details.map((i: ValidationErrorItem) => ({ field: i.context?.label, message: i.message }));

		return {
			value: null,
			error: {
				code: `producer/validation-error`,
				messages: message
			}
		};
	}

	return {
		value,
		error: null
	};
};
