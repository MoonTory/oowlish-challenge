export const ErrorHandlerMiddleware = (err: any, req: any, res: any, next: any) => {
	if (err.code === 'auth/invalid-email-password') {
		err.status = 401;
	}

	if (res.headersSent) {
		return next(err);
	}

	// set locals, only providing error in development
	res.locals.message = err.message;
	res.locals.error = req.app.get('env') === 'development' ? err : {};

	// render the error page
	res.status(err.status || 500);
	res.json({
		error: {
			code: err.code ? err.code : undefined,
			message: err.message
		}
	});
};
