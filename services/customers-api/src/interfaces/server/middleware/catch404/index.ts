import { Response, NextFunction } from 'express';

import { Middleware } from '../../../../typings';

export class RequestError extends Error {
	public readonly status: number;
	constructor(message?: string, status?: number) {
		super(message);
		this.status = status ? status : 500;
	}
}

export const catch404: Middleware = () => (_: any, __: Response, next: NextFunction) => {
	const err = new RequestError('Not Found', 404);

	return next(err);
};
