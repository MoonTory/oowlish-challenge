import { Response, NextFunction } from 'express';
import mongoSanitize from 'mongo-sanitize';

import { Middleware } from '../../../../typings';

type ReqProperty = 'params' | 'body' | 'query';
export const sanitize: Middleware = (property: ReqProperty) => (req: any, _: Response, next: NextFunction) => {
	for (const [key, value] of Object.entries(req[property])) {
		req[property][key] = mongoSanitize(value);
		// Add other sanitization here
	}
	return next();
};
