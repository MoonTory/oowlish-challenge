import { EndpointFunction } from '../../../../typings';

export class HealthEndpoint extends EndpointFunction {
	constructor() {
		super([]);
	}

	protected async executeImpl(): Promise<void | any> {
		return this.ok({ message: 'Healthy!' });
	}
}
