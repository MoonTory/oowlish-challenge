import { EndpointFunction, IContainer, ICustomerBusiness, TYPES } from '../../../../../typings';

export class TotalCustomersByCityEndpoint extends EndpointFunction {
	private readonly _container: IContainer;

	constructor(container: IContainer) {
		super([]);
		this._container = container;
	}

	protected async executeImpl(): Promise<void | any> {
		const retrieve = await this._container.resolve<ICustomerBusiness>(TYPES.CustomerBusiness).totalCustomersByCity(this.req.params.city);

		if (retrieve.isFailure) return this.conflict(retrieve.error);

		return this.ok(retrieve.result);
	}
}
