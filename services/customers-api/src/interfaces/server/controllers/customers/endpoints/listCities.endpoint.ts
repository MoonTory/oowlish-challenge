import { EndpointFunction, IContainer, ICustomerBusiness, TYPES } from '../../../../../typings';

export class ListCitiesEndpoint extends EndpointFunction {
	private readonly _container: IContainer;

	constructor(container: IContainer) {
		super([]);
		this._container = container;
	}

	protected async executeImpl(): Promise<void | any> {
		const list = await this._container.resolve<ICustomerBusiness>(TYPES.CustomerBusiness).listCities();

		if (list.isFailure) return this.processError(list.error);

		return this.ok(list.result);
	}

	private processError(error: { code: string; message: string }) {
		switch (error.code) {
			default:
				return this.jsonResponse(500, error, 'error');
		}
	}
}
