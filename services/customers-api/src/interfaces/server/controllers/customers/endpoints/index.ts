import { FindByIdEndpoint } from './findById.endpoint';
import { ListEndpoint } from './list.endpoint';
import { ListCitiesEndpoint } from './listCities.endpoint';
import { TotalCustomersByCityEndpoint } from './totalCustomersByCity.endpoint';

export const CustomersEndpoints = {
	FindByIdEndpoint,
	ListEndpoint,
	ListCitiesEndpoint,
	TotalCustomersByCityEndpoint
};
