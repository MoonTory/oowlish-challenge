import { validate, JoiSchemas, sanitize } from '../../../middleware';
import { EndpointFunction, IContainer, ICustomerBusiness, TYPES } from '../../../../../typings';

const schemas = JoiSchemas.query.paginate.page.concat(JoiSchemas.query.paginate.limit).concat(JoiSchemas.query.city);
export class ListEndpoint extends EndpointFunction {
	private readonly _container: IContainer;

	constructor(container: IContainer) {
		super([validate(schemas, 'query')]);
		this._container = container;
	}

	protected async executeImpl(): Promise<void | any> {
		const retrieve = await this._container
			.resolve<ICustomerBusiness>(TYPES.CustomerBusiness)
			.retrieve(this.req.query.city ? { city: this.req.query.city as string | undefined } : undefined, {
				page: this.req.query.page as number | undefined,
				limit: this.req.query.limit as number | undefined
			});

		if (retrieve.isFailure) return this.conflict(retrieve.error);

		return this.ok(retrieve.result);
	}
}
