import { EndpointFunction, IContainer, ICustomerBusiness, TYPES } from '../../../../../typings';

export class FindByIdEndpoint extends EndpointFunction {
	private readonly _container: IContainer;

	constructor(container: IContainer) {
		super([]);
		this._container = container;
	}

	protected async executeImpl(): Promise<void | any> {
		const find = await this._container.resolve<ICustomerBusiness>(TYPES.CustomerBusiness).findById(this.req.params.id);

		if (find.isFailure) return this.processError(find.error);

		return this.ok(find.result);
	}

	private processError(error: { code: string; message: string }) {
		switch (error.code) {
			case 'customer/not-found':
				return this.notFound(error);
			default:
				return this.jsonResponse(500, error, 'error');
		}
	}
}
