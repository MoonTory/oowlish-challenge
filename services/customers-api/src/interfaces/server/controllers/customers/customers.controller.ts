import { HealthEndpoint } from './health.endpoint';
import { CustomersEndpoints } from './endpoints';

import { IController, IConfig, EndpointMap, IContainer } from '../../../../typings';

export default class CustomersController extends IController<IConfig> {
	private readonly _endpoints: EndpointMap;

	constructor(path: string, config: IConfig, container: IContainer) {
		super(path, config);

		this._endpoints = {
			health: new HealthEndpoint(),
			list: new CustomersEndpoints.ListEndpoint(container),
			findById: new CustomersEndpoints.FindByIdEndpoint(container),
			listCities: new CustomersEndpoints.ListCitiesEndpoint(container),
			totalCustomersByCity: new CustomersEndpoints.TotalCustomersByCityEndpoint(container)
		};

		this.init();
	}

	public async init() {
		this.router.get(this._path + '/health', ...this._endpoints['health'].middlewares, this._endpoints['health'].execute());

		this.router.get(this._path, ...this._endpoints['list'].middlewares, this._endpoints['list'].execute());
		this.router.get(this._path + '/city', ...this._endpoints['listCities'].middlewares, this._endpoints['listCities'].execute());
		this.router.get(
			this._path + '/city/:city',
			...this._endpoints['totalCustomersByCity'].middlewares,
			this._endpoints['totalCustomersByCity'].execute()
		);
		this.router.get(this._path + '/:id', ...this._endpoints['findById'].middlewares, this._endpoints['findById'].execute());

		console.log(this._path + ' ' + 'Initialized successfully...');
	}
}
