/**
 * Node_Module dependencies.
 */
import express, { urlencoded, json } from 'express';
import compression from 'compression';
import morgan from 'morgan';
import helmet from 'helmet';
import cors from 'cors';
import cookieParser from 'cookie-parser';
import rateLimit from 'express-rate-limit';
import slowDown from 'express-slow-down';
import useragent from 'express-useragent';

// Middlewares
import { ErrorHandlerMiddleware } from './middleware';

import { HTTP } from './http';
import { API } from './api';
import { IConfig, AbstractServer, IContainer } from '../../typings';
import { catch404 } from './middleware/catch404';

const rateLimiter = rateLimit({
	windowMs: 10 * 60 * 1000, // 10 minutes
	max: 100000, // limit each IP to 10.000 requests per minute windowMs
	message: 'Too many requests from this IP. Please try again later'
});

const speedLimiter = slowDown({
	windowMs: 10 * 60 * 1000, // 10 minutes
	delayAfter: 50000, // allow 5.000 requests per 10 minutes, then...
	delayMs: 50, // begin adding 50ms of delay per request above 5.000:
	onLimitReached: (req, _, __) => {
		console.log('User blocked for flooding: ' + (req.headers['x-forwarded-for'] || req.connection.remoteAddress));
	}
});

export class HTTPServer extends AbstractServer<IConfig> {
	private static _instance: HTTPServer;
	private _http: HTTP;
	private _api: API;
	private _express: express.Application;

	private constructor(config: IConfig, container: IContainer) {
		super(config);
		this._express = express();

		if (this._config.APP_PORT) {
			this._express.set('port', this._config.APP_PORT);
		}

		this._http = HTTP.getInstance(this._express, this._config.APP_PORT);
		this._api = API.getInstance(this._config, container);

		this.config();
	}

	public static getInstance(config: IConfig, container: IContainer): HTTPServer {
		if (!HTTPServer._instance) {
			HTTPServer._instance = new HTTPServer(config, container);
			// ... any one time initialization goes here ...
		}
		return HTTPServer._instance;
	}

	public async listen() {
		await this._http.listen(this._http.port(), () => console.log(`Server start @ http://localhost:${this._http.port()} ...`));
	}

	protected async config() {
		this._express.use(helmet());
		this._express.use(cors<any>());
		this._express.use(compression());
		this._express.use(json());
		this._express.use(urlencoded({ extended: false }));
		this._express.use(cookieParser());
		this._express.use(morgan<any, any>(this._config.NODE_ENV === 'development' ? 'dev' : 'prod'));

		this._express.use(useragent.express());

		this._express.use(rateLimiter);
		this._express.use(speedLimiter);

		this._express.use(this._api.router);

		this._express.use(catch404());
		this._express.use(ErrorHandlerMiddleware);
	}
}
