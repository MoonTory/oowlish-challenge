import { Router } from 'express';

import { IController, IConfig, IContainer } from '../../../typings';
import CustomersController from '../controllers/customers';

export class API {
	private static _instance: API;
	private readonly _path: string;
	private readonly _config: IConfig;
	public router: Router;

	private constructor(config: IConfig, container: IContainer) {
		this._config = config;
		this._path = `/v${this._config.API_VERSION}`;
		this.config(container);
	}

	private async config(container: IContainer) {
		this.router = Router();
		this.initialize([new CustomersController('customers', this._config, container)]);
	}

	private initialize(controllers: IController<any>[]) {
		controllers.forEach((el: IController<any>) => {
			this.router.use(this._path, el.router);
		});
	}

	public static getInstance(config: IConfig, container: IContainer): API {
		if (!API._instance) {
			API._instance = new API(config, container);
		}
		return API._instance;
	}
}
