import { Result } from '@oowlish-challenge/types';
import { Schema, ValidationErrorItem } from 'joi';

export const validateSchema = <T = any>(payload: any, schema: Schema): Result<T> => {
	const { error, value } = schema.validate(payload);

	if (error) {
		const { details } = error;
		const message = details.map((i: ValidationErrorItem) => ({ field: i.context?.label, message: i.message }));

		return Result.fail(message);
	}

	return Result.ok(value);
};
