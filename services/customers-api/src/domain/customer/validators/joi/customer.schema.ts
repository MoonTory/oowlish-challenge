/* eslint-disable prettier/prettier */
import joi from 'joi';

const JoiString = (name: string) =>
	joi
		.string()
		.required()
		.messages({
			'string.base': `Invalid type, ${name} must be a string`,
			'string.empty': `${name} must not be empty`,
			'any.required': `${name} is required`
		});

const JoiNumber = (name: string) =>
	joi
		.number()
		.required()
		.messages({
			'number.base': `Invalid type, ${name} must be a string`,
			'any.required': `${name} is required`
		});

const JoiDate = joi.date().messages({
	'date.base': 'Invalid type, must be a date'
});

export const customer = joi.object().keys({
	first_name: JoiString('first_name'),
	last_name: JoiString('last_name'),
	email: JoiString('email'),
	gender: JoiString('gender'),
	company: JoiString('company'),
	city: JoiString('city'),
	title: JoiString('title'),
	lat: JoiNumber('lat'),
	long: JoiNumber('long'),
	datetime_created: JoiDate,
	datetime_updated: JoiDate
});
