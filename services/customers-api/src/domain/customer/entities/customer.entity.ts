import { ICustomerAttrs, ICustomerEntity, Result } from '@oowlish-challenge/types';

import { validateSchema, JoiSchemas } from '../validators';

import { Entity } from '../../../typings';

export class Customer extends Entity implements ICustomerEntity {
	public first_name: string;
	public last_name: string;
	public email: string;
	public gender: string;
	public company: string;
	public city: string;
	public title: string;
	public lat: number;
	public long: number;
	public datetime_created?: Date;
	public datetime_updated?: Date;

	private constructor(attrs: ICustomerAttrs, id?: string) {
		super(id);
		this.first_name = attrs.first_name;
		this.last_name = attrs.last_name;
		this.email = attrs.email;
		this.gender = attrs.gender;
		this.company = attrs.company;
		this.city = attrs.city;
		this.title = attrs.title;
		this.lat = attrs.lat;
		this.long = attrs.long;
		this.datetime_created = attrs.datetime_created ? attrs.datetime_created : new Date();
		this.datetime_updated = attrs.datetime_updated ? attrs.datetime_updated : new Date();
	}

	public static create(props: ICustomerAttrs, id?: string): Result<Customer> {
		const { isFailure, result, error } = validateSchema<ICustomerAttrs>(props, JoiSchemas.customer);

		if (isFailure) return Result.fail<Customer>(error);

		return Result.ok<Customer>(new Customer(result, id));
	}
}
