import { ICustomerEntity } from '@oowlish-challenge/types';

import { Schema, Model, model, Document, Types, PaginateModel } from 'mongoose';
import mongoosePaginate from 'mongoose-paginate-v2';
export interface ICustomerModel extends ICustomerEntity, Document {}

const ICustomerSchema = new Schema({
	id: { type: String, required: true },
	_id: { type: Types.ObjectId, default: Types.ObjectId },
	first_name: {
		type: String,
		required: true
	},
	last_name: {
		type: String,
		required: true
	},
	email: {
		type: String,
		required: true
	},
	gender: {
		type: String,
		required: true
	},
	city: {
		type: String,
		required: true
	},
	company: {
		type: String,
		required: true
	},
	title: {
		type: String,
		required: true
	},
	lat: {
		type: Number,
		required: true
	},
	long: {
		type: Number,
		required: true
	},
	datetime_created: {
		type: Date,
		required: true,
		default: new Date()
	},
	datetime_updated: {
		type: Date,
		required: true,
		default: new Date()
	}
});

ICustomerSchema.plugin(mongoosePaginate);

export const CustomerModel = model<ICustomerModel>('customers', ICustomerSchema) as PaginateModel<ICustomerModel>;
