import _ from 'lodash';
import { ICustomerAttrs, ICustomerEntity } from '@oowlish-challenge/types';
import { IContainer, ICustomerMapper, ICustomerRepository, PaginateResult, TYPES } from '../../../../typings';
import { ICustomerModel, CustomerModel, Customer } from '../../entities';

export class CustomerRepository extends ICustomerRepository {
	constructor(private _container: IContainer) {
		super(CustomerModel);
	}

	create = async (item: Customer) => {
		return await this._model.create(item);
	};

	update = async (query: any, item: Partial<ICustomerAttrs>) => {
		return await this._model.update(query, item);
	};

	list = async (query?: any) => {
		return (await this._model.find(query)).map((doc: ICustomerModel) => this._container.resolve<ICustomerMapper>(TYPES.CustomerMapper).toDomain(doc));
	};

	retrieve = async (query: any, options?: { page?: number; limit?: number; offset?: number }) => {
		const result = await this._model.paginate(query, _.omitBy(options, _.isNil));

		return {
			docs: result.docs.map((doc: ICustomerModel) => this._container.resolve<ICustomerMapper>(TYPES.CustomerMapper).toDomain(doc)),
			totalDocs: result.totalDocs,
			offset: result.offset,
			limit: result.limit,
			totalPages: result.totalPages,
			page: result.page,
			pagingCounter: result.pagingCounter,
			hasPrevPage: result.hasPrevPage,
			hasNextPage: result.hasNextPage,
			prevPage: result.prevPage,
			nextPage: result.nextPage
		} as PaginateResult<ICustomerEntity>;
	};

	listCities = async () => {
		const cities = await this._model.distinct('city');

		return cities;
	};

	listByCity = async (city: string) => {
		return (await this._model.find({ city })).map((item: any) => this._container.resolve<ICustomerMapper>(TYPES.CustomerMapper).toDomain(item));
	};

	findById = async (id: string) => {
		const customer = await this._model.findOne({ id: id.toString() });

		if (!customer) return null;

		return this._container.resolve<ICustomerMapper>(TYPES.CustomerMapper).toDomain(customer);
	};

	delete = async (id: string) => {
		const deleteOp = await this._model.deleteOne({ id });

		return deleteOp.n === deleteOp.ok;
	};
}
