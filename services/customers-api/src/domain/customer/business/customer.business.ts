import { ICustomerEntity, ITotalCustomersByCityView, OperationResult } from '@oowlish-challenge/types';
import { ICustomerBusiness, IConfig, IContainer, ICustomerRepository, TYPES } from '../../../typings';

export class CustomerBusiness extends ICustomerBusiness {
	private readonly _repo: ICustomerRepository;

	constructor(config: IConfig, container: IContainer) {
		super(config);

		this._repo = container.resolve<ICustomerRepository>(TYPES.CustomerRepository);
	}

	public create = async (item: ICustomerEntity) => {
		const customer = await this._repo.create(item);

		return OperationResult.ok(customer);
	};

	public listCities = async () => {
		const result: ITotalCustomersByCityView[] = [];
		const cities = await this._repo.listCities();

		for (const city of cities) {
			result.push((await this.totalCustomersByCity(city)).result);
		}

		return OperationResult.ok(result);
	};

	public totalCustomersByCity = async (city: string) => {
		const customers = await this._repo.list({ city });

		return OperationResult.ok({ city, customers_total: customers.length } as ITotalCustomersByCityView);
	};

	public findById = async (id: string) => {
		const customer = await this._repo.findById(id);

		if (!customer) return OperationResult.fail<ICustomerEntity>({ code: 'customer/not-found', message: `No customer found for id: ${id}` });

		return OperationResult.ok(customer);
	};

	public retrieve = async (query?: any, options?: { page?: number; limit?: number; offset?: number }) => {
		const customers = await this._repo.retrieve(query, options);

		return OperationResult.ok(customers);
	};
}
