/* eslint-disable prettier/prettier */
import { TotalCustomersByCityView } from './totalCustomersByCity.view';

describe('TotalCustomersByCityView', function () {
	it('should create a valid view', function () {
		const props = { city: 'Atlanta, GA', customers_total: 42 };

		const view = new TotalCustomersByCityView(props);

		expect(view.city).toBe(props.city);
		expect(view.customers_total).toBe(props.customers_total);
		expect(view).toBeInstanceOf(TotalCustomersByCityView);
	});
});
