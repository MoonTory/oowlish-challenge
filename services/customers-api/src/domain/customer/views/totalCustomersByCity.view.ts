import { ITotalCustomersByCityView } from '@oowlish-challenge/types';

export class TotalCustomersByCityView implements ITotalCustomersByCityView {
	private _city: string;
	private _customers_total: number;

	constructor({ city, customers_total }: ITotalCustomersByCityView) {
		this._city = city;
		this._customers_total = customers_total;
	}

	public get city() {
		return this._city;
	}

	public set city(value: string) {
		this._city = value;
	}

	public get customers_total(): number {
		return this._customers_total;
	}

	public set customers_total(v: number) {
		this._customers_total = v;
	}
}
