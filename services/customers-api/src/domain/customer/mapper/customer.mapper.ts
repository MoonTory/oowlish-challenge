import { ICustomerEntity } from '@oowlish-challenge/types';
import { ICustomerDTOEntity, ICustomerMapper } from '../../../typings';
import { Customer } from '../entities';

export class CustomerMapper implements ICustomerMapper {
	toDTO = (data: ICustomerEntity) => {
		return {
			id: data.id,
			lat: data.lat,
			long: data.long,
			first_name: data.first_name,
			last_name: data.last_name,
			city: data.city,
			title: data.title,
			email: data.email,
			gender: data.gender,
			company: data.company,
			datetime_created: data.datetime_created,
			datetime_updated: data.datetime_updated
		} as ICustomerDTOEntity;
	};

	toPersistence = (data: ICustomerEntity) => {
		return {
			id: data.id,
			lat: data.lat,
			long: data.long,
			first_name: data.first_name,
			last_name: data.last_name,
			city: data.city,
			title: data.title,
			email: data.email,
			gender: data.gender,
			company: data.company,
			datetime_created: data.datetime_created,
			datetime_updated: data.datetime_updated
		};
	};

	toDomain = (data: any) => {
		return {
			id: data.id,
			lat: data.lat,
			long: data.long,
			first_name: data.first_name,
			last_name: data.last_name,
			city: data.city,
			title: data.title,
			email: data.email,
			gender: data.gender,
			company: data.company,
			datetime_created: data.datetime_created,
			datetime_updated: data.datetime_updated
		} as ICustomerEntity;
	};
}
