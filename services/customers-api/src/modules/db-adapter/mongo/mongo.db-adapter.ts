import mongoose, { Mongoose } from 'mongoose';
import { MongoMemoryServer } from 'mongodb-memory-server';
import { IConfig, AbstractMongoDatabaseAdapter } from '../../../typings';

export class MongoDatabaseAdapter extends AbstractMongoDatabaseAdapter {
	private static _instance: MongoDatabaseAdapter;
	protected _connectionString: string;
	protected _connection: Mongoose;

	constructor(config: IConfig) {
		super(config);
	}

	public static getInstance(config: IConfig): MongoDatabaseAdapter {
		if (!MongoDatabaseAdapter._instance) {
			MongoDatabaseAdapter._instance = new MongoDatabaseAdapter(config);

			MongoDatabaseAdapter._instance._connectionString = MongoDatabaseAdapter._instance._config.DB_CONNECTION_STRING;
		}
		return MongoDatabaseAdapter._instance;
	}

	public get connection() {
		return this._connection;
	}

	public async connect(): Promise<void> {
		if (this.connection) {
			console.warn('MongoDB already connected...');
			return;
		}

		if (this._config.IN_MEMORY_DB) {
			const inMemoryServer = new MongoMemoryServer();
			this._connectionString = await inMemoryServer.getUri();
		}

		try {
			mongoose.connection.once('open', () => {
				console.log('MongoDB Connected...');
			});

			this._connection = await mongoose.connect(this._connectionString, { useNewUrlParser: true, useFindAndModify: false, useUnifiedTopology: true });
		} catch (error) {
			// Log Exception
			console.info('Retrying database connection in 10 seconds...');
			console.error(error);
			setTimeout(async () => {
				await MongoDatabaseAdapter._instance.connect();
			}, 10000);
		}
	}
}
