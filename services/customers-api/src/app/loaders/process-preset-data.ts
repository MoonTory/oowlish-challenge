import { Client } from '@googlemaps/google-maps-services-js';

import { Customer } from '../../domain';
import presetData from '../../config/customers.json';

const maps = new Client({});

const processCustomerData = async (customer: typeof presetData[0]) => {
	try {
		const geocode = await maps.geocode({
			params: { address: customer.city, key: process.env.GOOGLE_MAPS_API_KEY }
		} as any);

		const newCustomer = Customer.create(
			{
				first_name: customer.first_name,
				last_name: customer.last_name,
				email: customer.email,
				gender: customer.gender.toLowerCase(),
				city: customer.city,
				company: customer.company,
				title: customer.title,
				lat: geocode.data.results[0].geometry.location.lat,
				long: geocode.data.results[0].geometry.location.lng
			},
			customer.id.toString()
		);

		if (newCustomer.isFailure) {
			return { result: null, error: newCustomer.error };
		}

		return { result: newCustomer.getValue(), error: null };
	} catch (error) {
		return { result: null, error: { field: 'city', message: 'Could not locate the specified city' } };
	}
};

export const processPresetData = async (): Promise<Customer[]> => {
	const result: Customer[] = [];

	let batch: any[] = [];
	const batchSize = 10;
	const batches: Array<Customer[]> = [];

	for (let idx = 0; idx < presetData.length; idx++) {
		const customer = presetData[idx];

		if (idx % batchSize === 0) {
			batches.push(batch);
			batch = [];
		}

		batch.push(customer);
	}

	for (const batch of batches) {
		try {
			console.log('-- sending batch --');
			const ops = await Promise.all(batch.map((customer: any) => processCustomerData(customer)));

			for (const op of ops as any[]) {
				if (op.result === null) {
					console.log('op', op.error);
					continue;
				}

				result.push(op.result);
			}
		} catch (err) {
			console.error(err);
		}
	}

	return result;
};
