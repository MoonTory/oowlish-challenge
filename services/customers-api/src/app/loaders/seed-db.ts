import { CustomerModel } from '../../domain';
import { processPresetData } from './process-preset-data';

import mappedData from '../../../mapped_customers.json';

export const seedDBWithPresetData = async () => {
	await CustomerModel.insertMany(mappedData as any[]);
};
