import { CustomerBusiness, CustomerMapper, CustomerRepository } from '../domain';
import { Container } from '../container';
import { MongoDatabaseAdapter } from '../modules';
import { AbstractApplication, IConfig, IContainer, ICustomerMapper, ICustomerRepository, ICustomerBusiness, TYPES, IServer } from '../typings';
import { seedDBWithPresetData } from './loaders/seed-db';
import { HTTPServer } from '../interfaces';

export class App extends AbstractApplication<IConfig> {
	protected readonly _container: IContainer;
	protected readonly _server: IServer;

	constructor(config: IConfig) {
		super(config);
		this._db = new MongoDatabaseAdapter(this._config);

		this._container = new Container();

		this._container.bind<ICustomerMapper>(TYPES.CustomerMapper, new CustomerMapper());
		this._container.bind<ICustomerRepository>(TYPES.CustomerRepository, new CustomerRepository(this._container));
		this._container.bind<ICustomerBusiness>(TYPES.CustomerBusiness, new CustomerBusiness(this.config, this._container));

		this._server = HTTPServer.getInstance(this._config, this._container);
	}

	public start = async () => {
		console.log('Initializing...');
		await this._db.connect();
		await seedDBWithPresetData();

		await this._server.listen();
	};
}
