/* eslint-disable prettier/prettier */
import { App } from './app';

const test_config = {
	API_VERSION: '1',
	APP_PORT: 5001,
	NODE_ENV: 'test',
	DB_CONNECTION_STRING: '',
	IN_MEMORY_DB: true,
	GOOGLE_MAPS_API_KEY: ''
};

describe('App', function () {
	it('should start app and return app name', async function () {
		const app = new App(test_config);

		expect(app).toBeInstanceOf(App);
		expect(app.config).toBe(test_config);
	});
});
