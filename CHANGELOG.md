# Change Log

All notable changes to this project will be documented in this file.
See [Conventional Commits](https://conventionalcommits.org) for commit guidelines.

# 1.1.0 (2020-12-20)


### Features

* **customer-dash:** auth0 spa integration ([f26e2ac](https://github.com/MoonTory/oowlish-challenge/commit/f26e2acddc1912ada2fd84ec89aa6fdf24750ff1))
* dashboard boilerplate ([7d96815](https://github.com/MoonTory/oowlish-challenge/commit/7d9681564757dc31f5cf86911e6f50ff8082914c))
