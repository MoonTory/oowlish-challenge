export interface IResult<R> {
	isSuccess: boolean;
	isFailure: boolean;
	error: string | null;
	_value: R;
}

export class Result<T> {
	public isSuccess: boolean;
	public isFailure: boolean;
	public error: any | null;
	private _value: T;

	private constructor(isSuccess: boolean, error: string | null, value?: T) {
		if (isSuccess && error) {
			throw new Error(`InvalidOperation: A result cannot be
        successful and contain an error`);
		}
		if (!isSuccess && !error) {
			throw new Error(`InvalidOperation: A failing result
        needs to contain an error message`);
		}

		this.isSuccess = isSuccess;
		this.isFailure = !isSuccess;
		this.error = error;
		if (value) this._value = value;

		Object.freeze(this);
	}

	public getValue(): T {
		if (!this.isSuccess) {
			throw new Error(`Cant retrieve the value from a failed result.`);
		}

		return this._value;
	}

	public get result() {
		return this._value;
	}

	public static ok<U>(value?: U): Result<U> {
		return new Result<U>(true, null, value);
	}

	public static fail<U>(error: any): Result<U> {
		return new Result<U>(false, error);
	}

	public static combine(results: Result<any>[]): Result<any> {
		let payload = {};
		for (const result of results) {
			if (result.isFailure) return result;

			payload = { ...payload, ...result.getValue().props };
		}
		return Result.ok<any>(payload);
	}
}
