export interface IOperationResult<R> {
	isSuccess: boolean;
	isFailure: boolean;
	error: any | null | undefined;
	result: R;
}

export class OperationResult<T> implements IOperationResult<T> {
	public readonly isSuccess: boolean;
	public readonly isFailure: boolean;
	public readonly error: any | null | undefined;
	public readonly result: T;

	private constructor(isSuccess: boolean, error: any | null | undefined, value?: T) {
		if (isSuccess && error)
			throw new Error(`InvalidOperation: A result cannot be
        successful and contain an error`);

		if (!isSuccess && !error)
			throw new Error(`InvalidOperation: A failing result
        needs to contain an error message`);

		this.isSuccess = isSuccess;
		this.isFailure = !isSuccess;
		this.error = error;
		if (value) this.result = value;

		Object.freeze(this);
	}

	public static ok<U>(value?: U): OperationResult<U> {
		return new OperationResult<U>(true, null, value);
	}

	public static fail<U>(error: any): OperationResult<U> {
		return new OperationResult<U>(false, error);
	}
}
