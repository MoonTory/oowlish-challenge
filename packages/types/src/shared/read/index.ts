export interface IRead<T> {
	retrieve: (...args: any[]) => any;
	findById: (_id: string, ...args: any[]) => any;
}
