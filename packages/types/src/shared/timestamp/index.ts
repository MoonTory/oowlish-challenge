export interface ITimestamp {
	datetime_created?: Date;
	datetime_updated?: Date;
}
