export interface IWrite<T> {
	delete: (_id: string) => void;
	create: (item: T | any) => void;
	update: (query: any, item: any) => void;
}
