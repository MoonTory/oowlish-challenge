export interface ITotalCustomersByCityView {
	city: string;
	customers_total: number;
}
