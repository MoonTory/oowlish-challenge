import { IEntity, ITimestamp } from '../shared';

export type ICustomerEntity = ICustomerAttrs & IEntity;
export interface ICustomerAttrs extends ITimestamp {
	first_name: string;
	last_name: string;
	email: string;
	gender: string;
	company: string;
	city: string;
	title: string;
	lat: number;
	long: number;
}
